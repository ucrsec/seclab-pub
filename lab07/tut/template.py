#!/usr/bin/env python2

import struct
import subprocess as sp

def p32(n):
    return struct.pack("<I", n)

if __name__ == '__main__':
    p = sp.Popen("./crackme0x00-ssp-exec", stdin=sp.PIPE, stdout=sp.PIPE)
    print(p.stdout.readline())

    p.stdin.write("250382")
    while True:
        l = p.stdout.readline()
        l = l.strip()
        if l == "":
            break
        print(l)
    p.terminate()
