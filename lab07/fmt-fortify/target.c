#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <common.h>

char gflag[2048];

char *get_flag() {
  load_flag(gflag, sizeof(gflag));
  return gflag;
}

int main(int argc, char *argv[]) {
  /* find this pointer */
  char *pflag = get_flag();
  
  int seed = 0;
  for (int i = 0; i < strlen(pflag); i ++)
    seed += (int)pflag[i];

  printf("login (id = %08X): ", seed);

  char buf[100];
  if (scanf("%100s", &buf) != 1)
    err(1, "failed to get a string");
  
  printf("welcome, '");
  printf(buf);
  printf("'!\n");
}
