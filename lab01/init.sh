#!/bin/sh

ROOT=$(git rev-parse --show-toplevel)

$ROOT/bin/init

sudo apt-get -y install libc6-i386 > /dev/null

cat <<EOF
  _          _    __
 | |    __ _| |__/_ |
 | |   / _\` | '_ \| |
 | |__| (_| | |_) | |
 |_____\__,_|_.__/|_|
                    cs26002
EOF
