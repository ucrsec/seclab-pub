--------------------------------
Welcome to CS 260 02 Infosec Lab
--------------------------------

Introduction
============

This git repository will be your workspace to solve the labs and
problems for this course. It contains lab materials as well as
some utility scripts to help you with the lab.


File layouts
============

The file structure of this repo will look like the following:

    $ ls
    README		: this file
    bin/		: utility scripts
    labNN/		: materials for labNN

For each lab, it contains a README file as well as all challenges
as its subdirectories.

    $ ls lab01/
    README		: information about this lab
    bomblab1_01 : challenge bomblab1_01
    bomblab1_02 : challenge bomblab1_02

**Do read those README files as they contain very helpful information
about how to complete each lab and problem, including additional
reference materials and readings.**


Commmands
=========

    ; get new labs and problems
    $ git pull

    ; initialize your environment
    ; (this only has to be done once if you do not change the machine)
    $ ./bin/init

    ; submit flag
    $ ./bin/submit -l <lab-name> -p <problem-name> -f <path-to-file>

    ; submit writeup
    $ ./bin/submit -l <lab-name> -p <problem-name> -e <path-to-file>

