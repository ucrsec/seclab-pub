#!/bin/bash
ROOT=$(git rev-parse --show-toplevel)
HERE=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
if [[ ! $HERE == /home/vagrant/cs26002/* ]]
then
	echo "[ERR] move directory to /home/vagrant/cs26002"
	exit -1
fi

$ROOT/bin/init
echo core |sudo tee /proc/sys/kernel/core_pattern
sudo $ROOT/bin/setup disable_aslr
sudo $ROOT/bin/setenv --dir $HERE

cat <<EOF
  _       _     _  _
 | | __ _| |__ | || |
 | |/ _\` | '_ \| || |_
 | | (_| | |_) |__   _|
 |_|\__,_|_.__/   |_|

                  cs26002
EOF
