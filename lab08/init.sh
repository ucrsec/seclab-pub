#!/bin/bash
ROOT=$(git rev-parse --show-toplevel)
HERE=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

$ROOT/bin/init

install_libheap() {
    python -c "import libheap" 2>&1 >/dev/null
    if [ $? -eq 1 ];
    then
        sudo apt-get install -y python-pip libc6-dbg libc6-dbg:i386
        git clone https://github.com/cloudburst/libheap ~/libheap
        sudo pip install ~/libheap
        # patch libc version
        sudo sed -i 's/2.23/2.19/' /usr/local/lib/python2.7/dist-packages/libheap/libheap.cfg
        echo "python from libheap import *" >> ~/.gdbinit

        # patch peda
        curl -o ~/pedacolor.diff https://gist.githubusercontent.com/cloudburst/f5b6ec91b1e2fdad9ebb4b77bb13fece/raw/4c7a5ad5dba9e6bfacd6a13195394817caea7e86/pedacolor.diff
        patch ~/peda/peda.py ~/pedacolor.diff
    fi
}
install_libheap

# enable randomization
sudo $ROOT/bin/setup enable_aslr
sudo $ROOT/bin/setenv --dir $HERE

cat <<EOF
 _       _      ___
| | __ _| |__  ( _ )
| |/ _\` | '_ \ / _ \\
| | (_| | |_) | (_) |
|_|\__,_|_.__/ \___/
                  cs26002
EOF
