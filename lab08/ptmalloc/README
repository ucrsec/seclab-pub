Lab08: ptmalloc
===============

  glibc's malloc(), a variant of dlmalloc, called ptmalloc2, has a full
  set of protections. To get a flag, you probably have to bypass below
  errors.
  
  (see malloc.c)
     3348: errstr = "malloc(): memory corruption (fast)";
     3382: errstr = "malloc(): smallbin double linked list corrupted";
     3605: errstr = "malloc(): corrupted unsorted chunks";
     3712: errstr = "malloc(): corrupted unsorted chunks 2";
     3827: errstr = "free(): invalid pointer";
     3838: errstr = "free(): invalid size";
     3875: errstr = "free(): invalid next size (fast)";
     3900: errstr = "double free or corruption (fasttop)";
     3915: errstr = "invalid fastbin entry (free)";
     3936: errstr = "double free or corruption (top)";
     3944: errstr = "double free or corruption (out)";
     3950: errstr = "double free or corruption (!prev)";
     3958: errstr = "free(): invalid next size (normal)";
     3993: errstr = "free(): corrupted unsorted chunks";
     4214: errstr = "realloc(): invalid old size";
     4230: errstr = "realloc(): invalid next size";
  
  Here is a useful tip to debug the malloc/free() calls by
  using villoc.py.
  
  $ ltrace ./target 2>trace.log
  ...
  $ bin/villoc.py trace.log trace.html
  
  The target binary is:
  
      CANARY    : ENABLED
      FORTIFY   : disabled
      NX        : ENABLED
      PIE       : ENABLED
      RELRO     : FULL

- target    : target
- type      : local
- arch      : 32
- level     : 15
- points    : 20
- uid       : 28015
- release   : 03-10-2017 00:00:00
- deadline  : 03-17-2017 00:00:00
- hashval   : EE1BFB94
