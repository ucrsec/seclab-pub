from testcases.test_general import *
from testcases.test_x86_64 import *
from testcases.test_x86 import *
from testcases.test_arm import *
from testcases.test_mips import *
from testcases.test_ppc import *
from testcases.test_console import *
import unittest

print('Testcases are working with capstone 3.0.4')
unittest.main()
