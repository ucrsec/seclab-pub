#!/bin/sh

ROOT=$(git rev-parse --show-toplevel)

$ROOT/bin/init
sudo $ROOT/bin/setup disable_aslr

sudo dpkg --add-architecture i386
sudo apt-get -qq update
sudo apt-get -q install -y libseccomp-dev libseccomp-dev:i386

cat <<EOF
 _          _    ____  
| |    __ _| |__|___ \ 
| |   / _\` | '_ \ __) |
| |__| (_| | |_) / __/ 
|_____\__,_|_.__/_____|
                    cs26002
EOF
