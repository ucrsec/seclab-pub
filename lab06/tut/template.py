#!/usr/bin/env python2

import struct
import subprocess as sp

def p32(n):
    return struct.pack("<I", n)

def read_addr(p):
    return int(p.stdout.readline().split(":")[1], 16)

if __name__ == '__main__':
    p = sp.Popen("./crackme0x00", stdin=sp.PIPE, stdout=sp.PIPE)

    # your addresses
    stack  = read_addr(p)
    start  = read_addr(p)
    printf = read_addr(p)

    # read IOLI
    p.stdout.readline()         

    buf = ["AAAAAAAAAAAAA" + "\n"]
    p.stdin.write("".join(buf))
    while True:
        l = p.stdout.readline()
        l = l.strip()
        if l == "":
            break
        print(l)
    p.terminate()
