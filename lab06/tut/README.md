======================
Lec06: Bypassing ASLR
======================

In this tutorial, we are going to learn some basic techniques to
bypass ASLR.

1. Position Independent Executable
----------------------------------

An updated crackme0x00, print out address of two functions, one local,
one imported.

    void start() {
      printf("IOLI Crackme Level 0x00\n");
      printf("Password:");

      char buf[32];
      memset(buf, 0, sizeof(buf));
      read(0, buf, 256);

      if (!strcmp(buf, "250382"))
        write(1, "Password OK :)\n", 15);
      else
        write(1, "Invalid Password!\n", 18);
    }

    int main(int argc, char *argv[])
    {
      setvbuf(stdout, NULL, _IONBF, 0);
      setvbuf(stdin, NULL, _IONBF, 0);

      void *self = dlopen(NULL, RTLD_NOW);
      printf("stack   : %p\n", &argc);
      printf("start() : %p\n", &start);
      printf("printf(): %p\n", dlsym(self, "printf"));

      start();

      return 0;
    }

Now let's compile it.

    $ make
    cc -m32 -g -O0 -fno-stack-protector -o crackme0x00 crackme0x00.c -ldl
    /vagrant/bin/checksec.sh --file crackme0x00
    RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
    Partial RELRO   No canary found   NX enabled    No PIE          No RPATH   No RUNPATH   crackme0x00
    cc -m32 -g -O0 -fno-stack-protector -pie -o crackme0x00-pie crackme0x00.c -ldl
    /vagrant/bin/checksec.sh --file crackme0x00
    RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
    Partial RELRO   No canary found   NX enabled    PIE enabled     No RPATH   No RUNPATH   crackme0x00-pie

NOTE. The PIE column is different, what does this mean?

    $ ./crackme0x00
    stack   : 0xffe8c8c0
    start() : 0x804867b
    printf(): 0xf7626670
    IOLI Crackme Level 0x00
    Password:a
    Invalid Password!

    $ ./crackme0x00
    stack   : 0xffcf13f0
    start() : 0x804867b
    printf(): 0xf760b670
    IOLI Crackme Level 0x00
    Password:b
    Invalid Password!

    $ ./crackme0x00
    stack   : 0xff937930
    start() : 0x804867b
    printf(): 0xf75c5670
    IOLI Crackme Level 0x00
    Password:c
    Invalid Password!

NOTE. While the address of stack and `printf()` is changing, the
address of `start()` isn't.

Now let's try the one with PIE enabled.

    $ ./crackme0x00-pie
    stack   : 0xff960d90
    start() : 0x5664a8b0
    printf(): 0xf7611670
    IOLI Crackme Level 0x00
    Password:a
    Invalid Password!

    $ ./crackme0x00-pie
    stack   : 0xfffdad40
    start() : 0x565778b0
    printf(): 0xf7631670
    IOLI Crackme Level 0x00
    Password:b
    Invalid Password!

    $ ./crackme0x00-pie
    stack   : 0xff95c6d0
    start() : 0x566388b0
    printf(): 0xf75ae670
    IOLI Crackme Level 0x00
    Password:c
    Invalid Password!

Position-independent executable (PIE) is a body of machine code that,
being placed somewhere in the primary memory, executes properly
regardless of its absolute address.

What makes an executable position independent?

    $ readelf -r crackme0x00

    Relocation section '.rel.dyn' at offset 0x440 contains 3 entries:
    Offset     Info    Type            Sym.Value  Sym. Name
    08049ffc  00000606 R_386_GLOB_DAT    00000000   __gmon_start__
    0804a040  00001005 R_386_COPY        0804a040   stdin@GLIBC_2.0
    0804a044  00000e05 R_386_COPY        0804a044   stdout@GLIBC_2.0
    ...

    $ readelf -r crackme0x00-pie

    Relocation section '.rel.dyn' at offset 0x4f0 contains 37 entries:
     Offset     Info    Type            Sym.Value  Sym. Name
    000008ba  00000008 R_386_RELATIVE
    000008ca  00000008 R_386_RELATIVE
    ...
    000008bf  00000602 R_386_PC32        00000000   puts@GLIBC_2.0
    00000920  00000602 R_386_PC32        00000000   puts@GLIBC_2.0
    00000932  00000602 R_386_PC32        00000000   puts@GLIBC_2.0
    ...
    00001fe8  00000306 R_386_GLOB_DAT    00000000   _ITM_deregisterTMClone
    00001fec  00000506 R_386_GLOB_DAT    00000000   __cxa_finalize@GLIBC_2.1.3
    ...

What is the purpose of these additional entries?

    $ gdb ./crackme0x00-pie
    (gdb) disass start
    Dump of assembler code for function start:
       0x000008b0 <+0>:     push   ebp
       0x000008b1 <+1>:     mov    ebp,esp
       0x000008b3 <+3>:     sub    esp,0x28
       0x000008b6 <+6>:     sub    esp,0xc
       0x000008b9 <+9>:     push   0xa70
       0x000008be <+14>:    call   0x8bf <start+15>
       0x000008c3 <+19>:    add    esp,0x10
    ...
    (gdb) start
    ...
    (gdb) disass start
    Dump of assembler code for function start:
       0x565558b0 <+0>:     push   ebp
       0x565558b1 <+1>:     mov    ebp,esp
       0x565558b3 <+3>:     sub    esp,0x28
       0x565558b6 <+6>:     sub    esp,0xc
       0x565558b9 <+9>:     push   0x56555a70
       0x565558be <+14>:    call   0xf7e55ca0 <puts>
    ...
    (gdb) info address puts
    Symbol "puts" is at 0xf7e55ca0 in a file compiled without debugging.

These entries are used by loader (`ld.so`) to patch the binary according
to its loaded address.

Check `R_386_32`, `R_386_GLOB_DAT`, and `R_386_RELATIVE`.

2. Dynamic Linking
------------------

In last week's tutorial, we learned how to use the address of `printf()`
to calculate the address of `system()` and `/bin/sh`, then craft the
ROP payload to get shell. If the address of `printf()` is not given,
can you exploit the weakness of non-PIE binary to find its address.

To do so, you need to understand another mechanism: dynamic loading,
i.e., how does `crackme0x00` knows where `printf()` is.

    $ gdb ./crackme0x00
    (gdb) disass start
    Dump of assembler code for function start:
       0x0804867b <+0>:     push   ebp
       0x0804867c <+1>:     mov    ebp,esp
       0x0804867e <+3>:     sub    esp,0x28
       0x08048681 <+6>:     sub    esp,0xc
       0x08048684 <+9>:     push   0x8048840
       0x08048689 <+14>:    call   0x8048510 <puts@plt>
       0x0804868e <+19>:    add    esp,0x10
       0x08048691 <+22>:    sub    esp,0xc
       0x08048694 <+25>:    push   0x8048858
       0x08048699 <+30>:    call   0x8048500 <printf@plt>
       0x0804869e <+35>:    add    esp,0x10
    ...

NOTE. `start` is not directly calling `printf()` but `printf@plt`.

What is `plt`? Procedure linkage table (plt) contains trampolines for
functions defined in dynamic libraries.

    (gdb) disass 0x8048500
    Dump of assembler code for function printf@plt:
       0x08048500 <+0>:     jmp    DWORD PTR ds:0x804a014
       0x08048506 <+6>:     push   0x10
       0x0804850b <+11>:    jmp    0x80484d0

The trampoline just jump to an address stored at `ds:0x804a014`, where
is this address and what is stored in it?

    (gdb) info symbol 0x804a014
    _GLOBAL_OFFSET_TABLE_ + 20 in section .got.plt

It's an address in the `.got.plt` section (you can also read this
using `readelf -r`). What does table contains?

    (gdb) x /xw 0x804a014
    0x804a014:      0x08048506

NOTE. Before running, this is the address of `printf@plt+6`, which
jumps to `0x80484d0`.

    (gdb) x /7i 0x80484d0
       0x80484d0:   push   DWORD PTR ds:0x804a004
       0x80484d6:   jmp    DWORD PTR ds:0x804a008
       0x80484dc:   add    BYTE PTR [eax],al
       0x80484de:   add    BYTE PTR [eax],al
       0x80484e0 <strcmp@plt>:      jmp    DWORD PTR ds:0x804a00c
       0x80484e6 <strcmp@plt+6>:    push   0x0
       0x80484eb <strcmp@plt+11>:   jmp    0x80484d0

This is actually the beginning of `plt` table.

Now let's see what will happen when we run the program.

    (gdb) start
    (gdb) x /xw 0x804a014
    0x804a014:      0x08048506
    (gdb) x /xw 0x804a008
    0x804a008:      0xf7fedf00
    (gdb) info symbol 0xf7fedf00
    _dl_runtime_resolve in section .text of /lib/ld-linux.so.2

Now let's see how the content of `0x804a014` changes.

    (gdb) watch *0x804a014
    (gdb) c
    ...
    Hardware watchpoint 2: *0x804a014

    Old value = 134513926
    New value = -136055184
    _dl_fixup
    ...
    (gdb) c
    Breakpoint 1, 0x08048500 in printf@plt ()
    (gdb) x /xw 0x804a014
    0x804a014:      0xf7e3f670
    (gdb) info symbol 0xf7e3f670
    printf in section .text of /lib/i386-linux-gnu/libc.so.6

Is this the same address as printed out by the program? Now you know
how dynamic link works and how you can read the address of libc
functions from the `.got.plt` table.


3. Leaking Address Information
------------------------------

In order to bypass ASLR and launch the ROP attack you learned last
week, you need to leak the address of libc function, like `printf`.
This can be done by writing the content of the `.got.plt` table:

    write(1, addr_of_got_entry, 4);

The payload should look like this:

    [buf  ]
    [.....]
    [ra   ] -> write
    [dummy]
    [arg1 ] -> 1
    [arg2 ] -> printf@got
    [arg3 ] -> 4

You already learned how to find the address of `printf@plt`:

    $ readelf -r crackme0x00 | grep printf
    0804a014  00000407 R_386_JUMP_SLOT   00000000   printf@GLIBC_2.0

The question is how to find the address of `write()` which is also a
libc function. But do you need to find its address in `libc.so`?

NOTE. You need to find the address in libc because `system()` is not
an imported function and `/bin/sh` is not part of the binary.

The answer is maybe not. If `write()` is an imported function, then
you can just use its trampoline in the `plt` table.

    $ objdump -D crackme0x00 | grep "write@plt"
    08048550 <write@plt>:
     804871e:       e8 2d fe ff ff          call   8048550 <write@plt>
     8048734:       e8 17 fe ff ff          call   8048550 <write@plt>

Now the payload looks like this:

    [buf  ]
    [.....]
    [ra   ] -> write@plt (0x8048550)
    [dummy]
    [arg1 ] -> 1
    [arg2 ] -> printf@got (0x804a014)
    [arg3 ] -> 4

Once the address is leaked, you can then use what you have learned to
craft the ROP payload and attack.

Question: how to load the second payload?
