#!/bin/bash
ROOT=$(git rev-parse --show-toplevel)
HERE=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
if [[ ! $HERE == /home/vagrant/cs26002/* ]]
then
	echo "[ERR] move directory to /home/vagrant/cs26002"
	exit -1
fi

$ROOT/bin/init

install_capstone() {  
  if [ $(dpkg-query -W -f='${Status}' libcapstone3 2>/dev/null | \
	grep -c "ok installed") -eq 0 ];
  then

    URL="http://www.capstone-engine.org/download/3.0.4/ubuntu-14.04/"
    wget $URL/libcapstone3_3.0.4-0.1ubuntu1_amd64.deb -O libcapstone3.deb
    wget $URL/python-capstone_3.0.4-0.1ubuntu1_amd64.deb -O python-capstone.deb

    sudo dpkg -i libcapstone3.deb
    sudo dpkg -i python-capstone.deb

    rm libcapstone3.deb
    rm python-capstone.deb

  fi
}

install_capstone

# enable randomization
echo core |sudo tee /proc/sys/kernel/core_pattern
sudo $ROOT/bin/setup enable_aslr
sudo $ROOT/bin/setenv --dir $HERE

cat <<EOF
 _          _      __
| |    __ _| |__  / /_
| |   / _\` | '_ \| '_ \\
| |__| (_| | |_) | (_) |
|_____\__,_|_.__/ \___/
                  cs26002
EOF
