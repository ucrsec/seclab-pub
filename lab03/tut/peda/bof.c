#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

// compiling option: gcc -fno-stack-protector -o bof bof.c

char* dovuln(const char *s)
{
    char buf[64];
    strcpy(buf, s);
    puts(buf);
    fflush(stdout);
    return strdup(buf);
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        puts("missing argument");
        exit(1);
    }

    dovuln(argv[1]);
    return 0;
}
