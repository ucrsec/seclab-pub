=======================
Lec03: Writing Exploits
=======================

1. First Exploit
----------------

### 1.1. Crash ###

Do you remember the crackme binaries (and its password)?

    $ cd /vagrant/cs26002
    $ git pull
    $ cd lab03/tut
    $ cp ../../lab01/tut/IOLI-crackme/crackme0x00 .

If you disassembled the binary, you might see these code snippet:

    $ objdump -d crackme0x00
    ...
    8048448:       8d 45 e8                lea    -0x18(%ebp),%eax
    804844b:       89 44 24 04             mov    %eax,0x4(%esp)
    804844f:       c7 04 24 8c 85 04 08    movl   $0x804858c,(%esp)
    8048456:       e8 d5 fe ff ff          call   8048330 <scanf@plt>
    ...

What's the value of `0x804858c`? Yes, "%s", which means the `scanf()` function
gets a string as an argument on `-0x18(%ebp)` location.

What happens if you provide a long string? Like below.

    $ echo AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA | ./crackme0x00
    IOLI Crackme Level 0x00
    Password: Invalid Password!
    Segmentation fault

There are a few ways to check the status of the last segmentation
fault:

1. checking logging messages

        $ dmesg | tail -1
        [237413.117757] crackme0x00[353]: segfault at 41414141 ip 0000000041414141 sp 00000000ff92aef0
        error 14 in libc-2.24.so[f7578000+1b3000]

2. running gdb

        $ echo AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA > input
        $ gdb ./crackme0x00
        > run <input
        Starting program: ./crackme0x00 <input
        IOLI Crackme Level 0x00
        Password: Invalid Password!

        Program received signal SIGSEGV, Segmentation fault.
        0x41414141 in ?? ()

### 1.2. Control EIP ###

Let's figure out which input tainted the instruction pointer.

    $ echo AAAABBBBCCCCDDDDEEEEFFFFGGGGHHHHIIIIJJJJ > input
    $ dmesg | tail -1
    [238584.915883] crackme0x00[1095]: segfault at 48484848 ip 0000000048484848 sp 00000000ffc32f80
    error 14 in libc-2.24.s

What's the current instruction pointer? You might need this help:

    $ man ascii

You can also figure out the exact shape of the stack frame by looking at
the instructions as well.

    $ objdump -d crackme0x00
    ...
    8048414:       55                      push   %ebp
    8048415:       89 e5                   mov    %esp,%ebp
    8048417:       83 ec 28                sub    $0x28,%esp
    ...
    8048448:       8d 45 e8                lea    -0x18(%ebp),%eax
    804844b:       89 44 24 04             mov    %eax,0x4(%esp)
    804844f:       c7 04 24 8c 85 04 08    movl   $0x804858c,(%esp)
    8048456:       e8 d5 fe ff ff          call   8048330 <scanf@plt>
    ...


               |<---0x18-->|+--- ebp
    top                     v
    [          [       ]   ][fp][ra]
    |<----   0x28  ------->|

0x18 + 4 = 28, which is exactly the length of "AAAABBBBCCCCDDDDEEEEFFFFGGGG"
the following "HHHH" will cover the `[ra]`.

In this tutorial, we are going to hijack the control flow of ./crackme0x00
by overwriting the instruction pointer. As a first step, let's make it print
out "Password OK :)"!

```
    8048469:       e8 e2 fe ff ff          call   8048350 <strcmp@plt>
    804846e:       85 c0                   test   %eax,%eax
    8048470:       74 0e                   je     8048480 <main+0x6c>
    8048472:       c7 04 24 96 85 04 08    movl   $0x8048596,(%esp)
    8048479:       e8 c2 fe ff ff          call   8048340 <printf@plt>
    804847e:       eb 0c                   jmp    804848c <main+0x78>
 -> 8048480:       c7 04 24 a9 85 04 08    movl   $0x80485a9,(%esp)
    8048487:       e8 b4 fe ff ff          call   8048340 <printf@plt>
    804848c:       b8 00 00 00 00          mov    $0x0,%eax
    8048491:       c9                      leave
    8048492:       c3                      ret
```

We are going to jump to 0x08048480 such that it prints out "Password OK :)".
Which characters in input should be changed to 0x08048480? Let me remind you
that x86 is a little-endian machine.

    $ hexedit input

"C-x" will save your modification.

    $ cat input | ./crackme0x00
    IOLI Crackme Level 0x00
    Password: Invalid Password!
    Password OK :)
    Segmentation fault

### 1.3. Exploit Script ###

Today's task is to create a python template for exploitation.

```python
#!/usr/bin/env python2

import os
import struct
import subprocess as sp

def p32(n):
    return struct.pack("<I", n)

def p64(n):
    return struct.pack("<Q", n)

if __name__ == '__main__':

    assert p32(0x12345678) == b'\x78\x56\x34\x12'
    assert p64(0x12345678) == b'\x78\x56\x34\x12\x00\x00\x00\x00'

    cmds = ["./crackme0x00"]
    env = os.environ
    input = "hello world\n"

    p = sp.Popen(cmds, env=env,
                 stdout=sp.PIPE, stdin=sp.PIPE,
                 universal_newlines=False)
    p.stdin.write(input)
    out = p.stdout.read()
    p.wait()

    print(out)
```

Please modify this python script to hijack the control flow of crackme0x00!

2. Creating exploit with PEDA
----------------------------------

### 2.1. Installation ###

PEDA: http://ropshell.com/peda/

    # install gdb with python2 binding
    $ cd /vagrant/cs26002
    $ git pull
    $ sudo apt-get remove gdb
    $ sudo dpkg -i bin/gdb_7.7.1-0ubuntu5~14.04.2_amd64.deb

    # install PEDA
    $ git clone https://github.com/longld/peda.git ~/peda
    $ echo "source ~/peda/peda.py" >> ~/.gdbinit

    # TIP. if you don't want to see gdb's init banner
    $ echo 'alias gdb="gdb -q"' >> ~/.bashrc

    # TIP. do you want to have a fancy prompt? check ~/peda/peda.py

### 2.2. Testing ###

    $ cd /vagrant/cs26002/lab03/tut/peda
    $ gdb ./bof
    ...
    gdb-peda$ start
    [----------------------------------registers-----------------------------------]
    EAX: 0x1
    EBX: 0xf7fc8000 --> 0x1aada8
    ECX: 0xdf5af6d6
    EDX: 0xffffd6f4 --> 0xf7fc8000 --> 0x1aada8
    ESI: 0x0
    EDI: 0x0
    EBP: 0xffffd6c8 --> 0x0
    ESP: 0xffffd6c8 --> 0x0
    EIP: 0x8048504 (<main+3>:       and    esp,0xfffffff0)
    EFLAGS: 0x246 (carry PARITY adjust ZERO sign trap INTERRUPT direction overflow)
    [-------------------------------------code-------------------------------------]
       0x8048500 <dovuln+60>:       ret
       0x8048501 <main>:    push   ebp
       0x8048502 <main+1>:  mov    ebp,esp
    => 0x8048504 <main+3>:  and    esp,0xfffffff0
       0x8048507 <main+6>:  sub    esp,0x10
       0x804850a <main+9>:  cmp    DWORD PTR [ebp+0x8],0x1
       0x804850e <main+13>: jg     0x8048528 <main+39>
       0x8048510 <main+15>: mov    DWORD PTR [esp],0x8048610
    [------------------------------------stack-------------------------------------]
    0000| 0xffffd6c8 --> 0x0
    0004| 0xffffd6cc --> 0xf7e36af3 (<__libc_start_main+243>:       mov    DWORD PTR [esp],eax)
    0008| 0xffffd6d0 --> 0x1
    0012| 0xffffd6d4 --> 0xffffd764 --> 0xffffd88b ("/vagrant/cs26002/lab03/tut/peda/bof")
    0016| 0xffffd6d8 --> 0xffffd76c --> 0xffffd8af ("XDG_SESSION_ID=3")
    0020| 0xffffd6dc --> 0xf7feacca (add    ebx,0x12336)
    0024| 0xffffd6e0 --> 0x1
    0028| 0xffffd6e4 --> 0xffffd764 --> 0xffffd88b ("/vagrant/cs26002/lab03/tut/peda/bof")
    [------------------------------------------------------------------------------]
    Legend: code, data, rodata, value

    Temporary breakpoint 1, 0x08048504 in main ()
    gdb-peda$

### 2.3. Useful commands ###

(https://github.com/longld/peda)
* `aslr` -- Show/set ASLR setting of GDB
* `checksec` -- Check for various security options of binary
* `dumpargs` -- Display arguments passed to a function when stopped at a call instruction
* `dumprop` -- Dump all ROP gadgets in specific memory range
* `elfheader` -- Get headers information from debugged ELF file
* `elfsymbol` -- Get non-debugging symbol information from an ELF file
* `lookup` -- Search for all addresses/references to addresses which belong to a memory range
* `patch` -- Patch memory start at an address with string/hexstring/int
* `pattern` -- Generate, search, or write a cyclic pattern to memory
* `procinfo` -- Display various info from /proc/pid/
* `pshow` -- Show various PEDA options and other settings
* `pset` -- Set various PEDA options and other settings
* `readelf` -- Get headers information from an ELF file
* `ropgadget` -- Get common ROP gadgets of binary or library
* `ropsearch` -- Search for ROP gadgets in memory
* `searchmem|find` -- Search for a pattern in memory; support regex search
* `shellcode` -- Generate or download common shellcodes.
* `skeleton` -- Generate python exploit code template
* `vmmap` -- Get virtual mapping address ranges of section(s) in debugged process
* `xormem` -- XOR a memory region with a key

Try.

    $ help peda

### 2.4. Video Tutorial ###

Follow the video tutorial to write an exploit for `bof`.

http://www.youtube.com/watch?v=knC3B3RKARo

Please also check:

http://ropshell.com/peda/Linux_Interactive_Exploit_Development_with_GDB_and_PEDA_Slides.pdf

We will introduce a few more PEDA's features in later labs.
