#!/bin/bash
ROOT=$(git rev-parse --show-toplevel)
HERE=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

$ROOT/bin/init
sudo $ROOT/bin/setup disable_aslr
sudo $ROOT/bin/setenv --dir $HERE

cat <<EOF
 _          _    _____ 
| |    __ _| |__|___ / 
| |   / _\` | '_ \ |_ \ 
| |__| (_| | |_) |__) |
|_____\__,_|_.__/____/ 
                    cs26002
EOF
